import sys
from os.path import join, isdir, isfile, dirname
def cur_file_dir():         # 找到正确路径
    path = sys.path[0]
    if isdir(path):
        return path
    elif isfile(path):
        return dirname(path)
def get_prelist(fname):         # 获取保留单词列表
    filename = join(cur_file_dir(), fname).replace('\\', '\\\\')
    fhandle = open(filename, 'r')
    wholestr = fhandle.read()
    prelist = wholestr.split(' ')
    for word in prelist:
        if word == "" or word == "\n" or word == "\t":
            prelist.remove(word)
    return prelist
def dialog_get():         # 通过对话框获取文件名
    app = wx.App()
    frame = wx.Frame(None)
    openwildcard = "All files(*.*)|*.*|" + \
                    "C/C++ files(*.c;*.cpp;*.h)|*.c;*cpp;*.h|" + \
                    "Java source files(*.java)|*.java|" + \
                    "Python source files(*.py)|*.py|" + \
                    "Text files(*.txt)|*.txt"
    openFileDialog = wx.FileDialog(frame, "Choose a file to open",         # “创建打开文件”对话框
                                   wildcard=openwildcard, 
                                   style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
    openFileDialog.ShowModal()
    dir = openFileDialog.GetDirectory()
    filename = openFileDialog.GetFilename()
    openFileDialog.Destroy()
    return dir, filename
