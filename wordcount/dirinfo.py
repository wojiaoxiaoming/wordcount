from wordcount import wordcount
from os import listdir
from os.path import isfile, join
class DirInfo:     # 用于处理目录的类
    def __init__(self, pathname):
        self.path = pathname
    def build_filelist(self, type=''):     # 在此目录中建立文件列表
        all_list = listdir(self.path)     # 从路径中获取所有文件和目录
        files = []     # 获取文件
        for file in all_list:
            if type != '':
                if isfile(join(self.path, file)) and file.endswith(type):
                    files.append(file)
            elif isfile(join(self.path, file)):
                files.append(file)
        return files
    def build_dirlist(self):     # 在此目录中建立目录列表
        all_list = listdir(self.path)     # 从路径中获取所有文件和目录
        dirs = []     # 获取文件
        for file in all_list:
            if isfile(join(self.path, file)) == False:
                dirs.append(file)
        return dirs  
    def build_infolist(self, type=''):     # 从文件列表建立信息列表
        filenames = self.build_filelist(type)
        dirnames = self.build_dirlist()
        infos = []     # 通过创建wordcount对象获取信息
        for fname in filenames:
            info = DirInfo(self.path, fname)
            infos.append(info)
        for dirname in dirnames:     # 处理内部目录
            new_path = join(self.path, dirname)
            newdir = DirInfo(new_path)
            new_infos = newdir.build_infolist(type)
            infos = infos + new_infos
        return infos
if __name__ == "__main__":
    import sys
    dirinfo = DirInfo(sys.path[0])
    for info in dirinfo.build_infolist('.txt'):
        print(info.fname)