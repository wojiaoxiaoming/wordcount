import sysdef CreateTxt():
    with open('C:\Users\Lenovo\Desktop\诗歌合集.txt','r',encoding='utf-8') as g_file:
        text= g_file.read()
    return text     #写入特定编码的文本文件，请给open()函数传入encoding参数，将字符串自动转换成指定编码  修改

def Process(lines):
    lines = lines.lower()
    for ch in ',*.""':
        lines = lines.replace(ch, " ")
    words = lines.split()
    return words

def CountLines():
    count = 0
    for index, line in enumerate(open('C:\\Users\\45899\\Untitled Folder\\test1.txt','r',encoding='utf-8')):
        count += 1
    print(count)

def WordCount():
    lines1 = CreateTxt()
    words1 = Process(lines1)
    counts = {}
    for word in words1:
        counts[word] = counts.get(word, 0) + 1
    print(counts)
    items = list(counts.items())
    items.sort(key=lambda x: x[1], reverse=True)

    for i in range(10):
        word, count = items[i]
        print("{0:<10}{1:>5}".format(word, count))

def AllWordCount():
    lines1 = CreateTxt()
    words1 = Process(lines1)
    counts = {}
    for word in words1:
        counts[word] = counts.get(word, 0) + 1
    print(counts)
    items = list(counts.items())
    items.sort(key=lambda x: x[1], reverse=True)
    for i in range(len(items)):
        word, count = items[i]
        print(word, count)


def SignalCount():
    letter = 0
    number = 0
    space = 0
    symbol = 0
    lines1 = CreateTxt()
    for char in lines1:
        if 'a' <= char <= 'z' or 'A' <= char <= 'Z':
            letter += 1
        elif char.isdigit():
            number += 1
        elif char in [' ', ' ']:
            space += 1
        else:
            symbol += 1
    print("字母数量，数字数量，空格数量，其他字符数量分别为：")
    print(letter, number, space, symbol)