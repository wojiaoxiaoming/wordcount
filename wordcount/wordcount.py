from os.path import join
import re
class wordcount:     # 用于获取此文件信息的类
    def __init__(self, _path, _fname):
        self.path = _path
        self.fname = _fname
        self.filename = join(self.path, self.fname).replace('\\', '\\\\')
    def char_num(self):     # 返回一个字符
        f = open(self.filename, 'r', encoding='utf-8')     # 打开名为“filename”的文件
        totalstr = f.read()
        f.close()
        return self.fname + ', 字符数：' + str(len(totalstr))
    def line_num(self):     # 返回文件的行号
        f = open(self.filename, 'r', encoding='utf-8')     # 打开名为“filename”的文件
        lines = f.readlines()
        f.close()
        return self.fname + ', 行数：' + str(len(lines))
    def word_num(self):     # 返回文件的字号
        f = open(self.filename, 'r')     # 打开名为“filename”的文件
        lines = f.readlines()
        f.close()
        linewords = []     # 通过正则表达式获取单词
        for i in range(0, len(lines)):
            linewords.append(re.findall(r'[A-Za-z]{2,}', lines[i]))
        count = 0     # 数一数字数
        for lineword in linewords:
            for word in lineword:
                if word.isalpha() == False or len(word) == 1:
                    lineword.remove(word)
            if lineword != ['']:
                count += len(lineword)
        return self.fname + ', 单词数：' + str(count)
    def count_num(self):
        f = open(self.filename, 'r')
        text=f.read()
        f.close()
        text.lower()
        text = text.replace('\n', '')
        text = text.replace('，', '')
        text = text.replace('。', '')
        list1 = text.split(" ")
        str={'a', 'of', 'in', 'an', 'was','are','on','in','to','this','that','for','by','from','but','with','and','the','his','their','they','had','as','were','could','not','The','at','be','after'}
        list2 = set(list1)
        list3 = set(list1)
        for s in list3:
            if s in str:
                list2.remove(s)
        dict = {}
        for word in list2:
            dict[word] = list1.count(word)
        word = list(dict.items())
        word.sort(key=lambda x: x[1], reverse=(True))
        for i in range(10):
            print(word[i])
    def line_detail(self):     # 文件返回行的详细信息
        f = open(self.filename, 'r', encoding='utf-8')     # 打开名为“filename”的文件
        lines = f.readlines()     # 将所有行字符串放入列表
        f.close()
        codelines, emptylines, commentlines = [], [], []     # 区分不同的线条
        for line in lines:
            tmpline = line.replace(' ', '')
            tmpline = tmpline.replace('\t', '')
            tmpline = tmpline.replace('\n', '')
            if len(tmpline) == 1 or len(tmpline) == 0:
                emptylines.append(line)
            elif tmpline.startswith('//'):
                commentlines.append(line)
            else:
                codelines.append(line)
        return self.fname + ', 代码行/空行/注释行：' + str(len(codelines)) + '/'\
                + str(len(emptylines)) + '/' + str(len(commentlines))
    def write_file(filename, info):     # 将信息写入文件
         f = open(filename, 'a')
         f.write(info)
         f.close()
if __name__ == '__main__':
    import sys
    nf = wordcount(sys.path[0], 'tox.txt')
    print(nf.char_num())
    print(nf.line_num())
    print(nf.word_num())
    print(nf.line_detail())
    print(nf.count_num())
